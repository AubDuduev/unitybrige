import UIKit

class LoadingViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModal: LoadingViewModal!
  
  //MARK: - Public variable
  
  
  //MARK: - Outlets
 
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModal()
   
  }
  private func initViewModal(){
    self.viewModal = LoadingViewModal(viewController: self)
  }
}
