
import Foundation

class LoadingViewModal: VMManagers {
  
  //MARK: - Public variable
  public var managers: LoadingManagers!
  public var VC      : LoadingViewController!
  
  
}
//MARK: - Initial
extension LoadingViewModal {
  
  convenience init(viewController: LoadingViewController) {
    self.init()
    self.VC       = viewController
    self.managers = LoadingManagers(viewModal: self)
  }
}
