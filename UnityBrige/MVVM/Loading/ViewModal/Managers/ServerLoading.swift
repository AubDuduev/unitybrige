import UIKit

class ServerLoading: VMManager {
  
  //MARK: - Public variable
  public var VM: LoadingViewModal!
  
  
}
//MARK: - Initial
extension ServerLoading {
  
  //MARK: - Inition
  convenience init(viewModal: LoadingViewModal) {
    self.init()
    self.VM = viewModal
  }
}


