import Foundation

class LogicLoading: VMManager {
  
  //MARK: - Public variable
  public var VM: LoadingViewModal!
  
  
}
//MARK: - Initial
extension LogicLoading {
  
  //MARK: - Inition
  convenience init(viewModal: LoadingViewModal) {
    self.init()
    self.VM = viewModal
  }
}
