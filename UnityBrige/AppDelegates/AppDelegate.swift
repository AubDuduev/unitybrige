//
//  AppDelegate.swift
//  UnityBrige
//
//  Created by Senior Developer on 30.12.2020.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
	
	
	@objc var currentUnityController: UnityController?
	@objc var rootViewController: UIViewController? {
		return window?.rootViewController
	}
	
	private let rootVC = RootVC()
	var window: UIWindow?
	
	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		//Set rootVC
		//self.rootVC.set(window: self.window)
		
		currentUnityController = UnityController()
		currentUnityController?.initUnity()
		window?.rootViewController = currentUnityController?.getUnityRootViewController()
		
		
		return true
	}
	
}

